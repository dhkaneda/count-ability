resource "random_pet" "bucket_name" {

}

output "random_pet_name" {
  value = random_pet.bucket_name.id
}

# Create a bucket
resource "aws_s3_bucket" "react_static_host" {
  bucket = "${var.app_name}-react-static-host-${random_pet.bucket_name.id}"
}

locals {
  build_folder_path = "../build"
  mime_types = {
    "css"  = "text/css"
    "html" = "text/html"
    "ico"  = "image/x-icon"
    "js"   = "text/javascript"
    "json" = "application/json"
    "png"  = "image/png"
    "txt"  = "text/plain"
  }
}

module "template_files" {
  source = "hashicorp/dir/template"

  base_dir = "${path.module}/build"
  # template_vars = {
  #   # Pass in any values that you wish to use in your templates.
  #   vpc_id = "vpc-abc123"
  # }
}

resource "aws_s3_object" "upload_build" {
  for_each = module.template_files.files

  bucket = aws_s3_bucket.react_static_host.bucket
  key    = each.key
  content_type = each.value.content_type

  source = each.value.source_path
  content = each.value.content

  etag   = each.value.digests.md5
}

resource "aws_s3_bucket_public_access_block" "public_access_block" {
  bucket = aws_s3_bucket.react_static_host.id

  block_public_acls       = false
  block_public_policy     = false
  ignore_public_acls      = false
  restrict_public_buckets = false
}

resource "aws_s3_bucket_policy" "public_access" {
  depends_on = [
    aws_s3_bucket_public_access_block.public_access_block
  ]
  bucket = aws_s3_bucket.react_static_host.bucket

  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect    = "Allow",
        Principal = "*",
        Action = [
          "s3:GetObject",
          "s3:ListBucket"
        ],
        Resource = [
          aws_s3_bucket.react_static_host.arn,
          "${aws_s3_bucket.react_static_host.arn}/*"
        ]
      }
    ]
  })
}

resource "aws_s3_bucket_website_configuration" "site_config" {
  bucket = aws_s3_bucket.react_static_host.id

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "index.html"
  }
}

